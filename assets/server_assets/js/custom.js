
/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 * 
 */

"use strict";

//filter
    $("#myFilter").on("change", function() {
    var value = $(this).val();
    $("#tbody tr").filter(function() {
      $(this).toggle($(this).text().indexOf(value) > -1)
    });
  });

// Disable Button
    $('#btn-delete').prop("disabled", true);
    $('#btn-edit').addClass("none-transition");

// Disable / Enable Button If Checkbox
    $('.check-item').click(function() {
      if ($('.check-item:checked').length == 1) {
        $('#btn-delete').prop("disabled", false);
        $('#btn-edit').prop("disabled", false);
      }else if ($('.check-item:checked').length > 1) {
          $('#btn-edit').prop("disabled", true);
      }else if($('.check-item:checked').length == 0) {
          $('#btn-delete').prop('disabled',true);
          $('#btn-edit').prop("disabled", true);
      }
    });
    $('.check-all').click(function() {
      if ($(this).is(':checked')) {
        $('#btn-delete').prop("disabled", false);
      } else {
      if ($('.check-all:checked').length < 1){
        $('#btn-delete').prop('disabled',true);
        }
      }
    });

// Check All
    $(".check-all").click(function(){ 
      if($(this).is(":checked"))
        $(".check-item").prop("checked", true); 
      else
        $(".check-item").prop("checked", false);
    });

// Confirm Delete
    $('#btn-delete').click(function() {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-delete').submit();
        }
      });
    });

    $('#btn-delete-enable').click(function() {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-delete').submit();
        }
      });
    });

// Alert
  $(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    });

  //Confirm Edit Profile
  $('#btn-edit-submit').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.value) {
          $('#form-edit').submit();
        }
      });
    });

  //Confirm Delete Profile+account
  $('#btn-delete-submit').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Hapus Data?',
        text: "Data yang terhapus tidak bisa dikembalikan!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya, Hapus!',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $('#form-delete').submit();
        }
      });
    });

   //Confirm Delete Profile+account
  $('#btn-confirm-submit').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya, Nonaktifkan!',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $('#form-konfirmasi').submit();
        }
      });
    });

   //Confirm Delete Profile+account
  $('#btn-nonactive-submit').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Nonaktifkan Akun?',
        text: "Akun yang dinonaktifkan tidak akan bisa diakses sementara!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#ff9b14',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya, Nonaktifkan!',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $('#form-nonaktif').submit();
        }
      });
    });

   //Confirm Delete Profile+account
  $('#btn-decline-submit').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-tolak').submit();
        }
      });
    });

  $('.btn-confirm-delete').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Anda Yakin?',
        text: "Anda tidak akan bisa mengembalikannya!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya, Hapus!',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $('#form-delete').submit();
        }
      });
    });

  $('.href-confirm-edit').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Anda Yakin?',
        text: "Anda bisa mengeditnya kembali!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya, Edit!',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $(this).parents('form').submit();
        }
      });
    });

  $('.href-confirm-delete').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Anda Yakin?',
        text: "Anda tidak akan bisa mengembalikannya!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Iya, Hapus!',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          var href = $('.href-hapus').attr('href');
          window.location = href;
        }
      });
    });

  $('.single-popup-photo').magnificPopup({
      type: 'image'
      // other options
    });
  $('.multi-popup-photo').magnificPopup({
      delegate: 'a', // child items selector, by clicking on it popup will open
      type: 'image'
      // other options
    });
