 <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright 2020 <strong>RPL Pasim</strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->

  <!-- Vendor JS Files -->
  <script src="<?= base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/php-email-form/validate.js"></script>
  <script src="<?= base_url() ?>assets/vendor/counterup/counterup.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/superfish/superfish.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/hoverIntent/hoverIntent.js"></script>
  <script src="<?= base_url() ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/venobox/venobox.min.js"></script>
  <script src="<?= base_url() ?>assets/vendor/aos/aos.js"></script>
  <script src="<?= base_url() ?>assets/vendor/magnific-popup//dist/jquery.magnific-popup.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url() ?>assets/js/custom.js"></script>
  <script src="<?= base_url() ?>assets/js/main.js"></script>
  <script src="<?= base_url('assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.js') ?>"></script>
    <script src="<?= base_url('assets/server_assets/js/alerts.js') ?>"></script>
  
    <script>
        $(document).ready(function(){
            <?php if ($this->session->flashdata('success')) : ?>
                showSuccessToast("<?= $this->session->flashdata('success'); ?>"); //function ada di assets/server_assets/js/alerts.js
            <?php endif; ?>

            <?php if ($this->session->flashdata('fail')) : ?>
                showDangerToast("<?= $this->session->flashdata('fail'); ?>");
            <?php endif; ?>

            <?php if ($this->session->flashdata('info')) : ?>
                showInfoToast("<?= $this->session->flashdata('info'); ?>");
            <?php endif; ?>

             <?php if ($this->session->flashdata('warning')) : ?>
                showWarningToast("<?= $this->session->flashdata('warning'); ?>");
            <?php endif; ?>

          });
    </script>


</body>

</html>