<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <?php foreach ($navbar as $d): ?>
      <title><?= $d->nama_sekolah.' - '. $title ?></title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url('assets/images/beranda_images/'.$d->ikon) ?>" rel="icon">
  <link href="<?= base_url('assets/images/beranda_images/'.$d->ikon) ?>" rel="apple-touch-icon">
    <?php endforeach ?>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url() ?>assets/vendor/magnific-popup/dist/magnific-popup.css">
  <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/server_assets/plugins/magnific-popup/dist/magnific-popup.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/icon-kit/dist/css/iconkit.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/ionicons/dist/css/ionicons.min.css">

  <!-- =======================================================
  * Template Name: Regna - v2.1.0
  * Template URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <style>
    #header {
    background: <?php if($this->uri->segment(1) == "" | $this->uri->segment(2) == "index"){echo "transparent";}else{echo "rgba(52, 59, 64, 0.9)";} ?>;
}
  </style>
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left row">
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt=""></a> -->
         <!-- Uncomment below if you prefer to use a text logo -->
         <style>
           @media (max-width: 768px) {
            #header #logo h1 {
              font-size: 26px;
              margin-left: 20px;
            }
            #header #logo img {
              max-height: 40px;
            }
          }
         </style>
         <?php foreach ($navbar as $d): ?>
          <h1 style="margin-top: -10px;"><a href="<?= base_url(); ?>"><?= $d->teks_logo ?></a></h1> 
         <?php endforeach ?>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li ><a href="<?= base_url() ?>">Beranda</a></li>
          <li class="menu-has-children <?= $this->uri->segment(3) == "detailStruktur" || $this->uri->segment(3) == "ListJurusan" ? 'menu-active' : ' '?>"><a href="#">Profil</a>
            <ul>
              <li><a href="<?= base_url('Page/detailPage/detailStruktur') ?>">Struktur Organisasi</a></li>
              <li><a href="<?= base_url('Page/detailPage/ListKelas') ?>">Nama Kelas</a></li>
            </ul>
          </li>
          <li class="menu-has-children <?= $this->uri->segment(3) == "ListFoto" || $this->uri->segment(3) == "ListVideo" ? 'menu-active' : ' '?>"><a href="#">Galeri</a>
            <ul>
              <li><a href="<?= base_url('Page/detailPage/ListFoto') ?>">Foto</a></li>
              <li><a href="<?= base_url('Page/detailPage/ListVideo') ?>">Video</a></li>
            </ul>
          </li>
          <li class="<?= $this->uri->segment(3) == "ListGuru" ? 'menu-active' : ' '?>"><a href="<?= base_url('Page/detailPage/ListGuru') ?>">Guru</a></li>
          <li class="<?= $this->uri->segment(3) == "ListPrestasi" ? 'menu-active' : ' '?>"><a href="<?= base_url('Page/detailPage/ListPrestasi') ?>">Prestasi</a></li>
          <li class="<?= $this->uri->segment(3) == "detailPpdb" ? 'menu-active' : ' '?>"><a href="<?= base_url('Page/detailPage/detailPpdb') ?>">PPDB</a></li>
          <li class="menu-has-children <?= $this->uri->segment(3) == "ListLowongan" || $this->uri->segment(3) == "ListSerapan" ? 'menu-active' : ' '?>"><a href="#">Ekstrakulikuler</a>
            <ul>
              <?php $kelas = $this->db->get('tb_m_eskul')->result();
                foreach ($kelas as $d) :
               ?>
              <li><a href="<?= base_url('Page/detailPageById/detailEskul/'.$d->id) ?>"><?= $d->judul ?></a></li>
             <?php endforeach; ?>
            </ul>
          </li>
          <li class="<?= $this->uri->segment(3) == "detailKontak"? 'menu-active' : ' ' ?>"><a href="<?= base_url('Page/detailPage/detailKontak') ?>">Hubungi Kami</a></li>
          <?php 
            $pathCaptcha = "./assets/captcha/";
            if (!file_exists($pathCaptcha)) {
              $createFolder = mkdir($pathCaptcha, 0772);
              if (!$createFolder) {
                return;
              }
            }

            $word = array_merge(range('0', '9'), range('A', 'Z'));
            $random = shuffle($word);
            $str = substr(implode($word), 0, 5);

            $dataSes = ["captcha_str" => $str];
            $this->session->set_userdata($dataSes);

            $vals = [
                  "word" => $str,
                  "img_path" => $pathCaptcha,
                  "img_url" => base_url("assets/captcha/"),
                  "img_width" => "150",
                  "img_height" => 40,
                  "expiration" => 7200
                ];
            $capc = create_captcha($vals);
            $captcha_image = $capc["image"];
           ?>
          <li class="menu-has-children"><a href="#">Login</a>
            <ul class="dropdown-menu dropdown-menu-right mt-2" style="width: 150px;">
               <li class="px-3 py-2">
                   <form action="<?= base_url('Page/login') ?>" method="POST">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control form-control-sm" name="email" value="<?= set_value('email'); ?>" autofocus>
                        <small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('email'); ?></small>
                      </div>
                      <div class="form-group mt-3">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control form-control-sm" name="password">
                        <small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('password'); ?></small>
                      </div>
                      <div class="form-group mt-3">
                          <span><?= $captcha_image; ?></span>
                          <a href="" onclick="parent.window.location.reload(true)">Perbarui Gambar</a>
                          <input class="form-control" name="captcha" id="password" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Captcha'" placeholder = 'Captcha' required>
                      </div>
                      <!-- <div class="row"> -->
                        <!-- <div class="text-left">
                          <a href="" ><span style="font-size: 10px;">Lupa password?</span></a>
                        </div> -->
                        <div class="ml-4 text-right">
                          <button type="submit" class="btn btn-primary text-sm-center mt-3 "><i class="ik ik-log-in"></i></button>
                        </div>
                      <!-- </div> -->
                    </form>
                </li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->
