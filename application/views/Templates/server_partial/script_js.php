     </div>
        <script src="<?= base_url() ?>assets/server_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/screenfull/dist/screenfull.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/moment/moment.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/d3/dist/d3.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/c3/c3.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/js/tables.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/js/widgets.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/js/charts.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/dist/js/theme.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/server_assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/plugins/summernote/dist/summernote-bs4.min.js"></script>
        
        <script src="<?= base_url() ?>assets/server_assets/plugins/datedropper/datedropper.min.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/js/form-picker.js"></script>
        <script src="<?= base_url() ?>assets/server_assets/js/custom.js"></script>
        <script>
            $(document).ready(function(){
                <?php if ($this->session->flashdata('success')) : ?>
                    showSuccessToast("<?= $this->session->flashdata('success'); ?>"); //function ada di assets/server_assets/js/alerts.js
                <?php endif; ?>

                <?php if ($this->session->flashdata('fail')) : ?>
                    showDangerToast("<?= $this->session->flashdata('fail'); ?>");
                <?php endif; ?>

                <?php if ($this->session->flashdata('info')) : ?>
                    showInfoToast("<?= $this->session->flashdata('info'); ?>");
                <?php endif; ?>

                 <?php if ($this->session->flashdata('warning')) : ?>
                    showWarningToast("<?= $this->session->flashdata('warning'); ?>");
                <?php endif; ?>

              });
        </script>
       
    </body>
</html>
