<!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details" data-aos="fade-up">
      <div class="row">
      	<?php foreach ($galeri as $d): ?>
		<?php if ($d->kategori == "foto"): ?>
      	<div class="col-12 col-md-6 col-lg-4">
	        <div class="portfolio-details-container card">

	          <div class="owl-carousel portfolio-details-carousel" >
	          	<a href="<?= base_url('assets/images/galeri_images/'.$d->item); ?>">
	            	<img src="<?= base_url('assets/images/galeri_images/'.$d->item) ?>" alt="Mobirise" title="" media-simple="true" width="100%" height="300" >
	       		</a>
	          </div>

	        </div>

	        <div class="portfolio-description" style="margin-top: -30px;">
	          <h2><?= $d->judul ?></h2>
	          <p>
	            <?= $d->deskripsi ?>
	          </p>
	        </div>
	    </div>
	    <?php endif ?>
	<?php endforeach ?>
      </div>
    </section><!-- End Portfolio Details Section
    -->
</div>