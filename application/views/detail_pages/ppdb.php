<div class="row mb-5">
	<div class="col-lg-8 col-6">
		<div class="col-lg-8">
			<form method="POST" action="<?= base_url('Page/insertPpdb'); ?>" enctype="multipart/form-data"> 
			<div class="form-group">
			    <label for="exampleFormControlFile1">Nama Siswa</label>
			    <input class="form-control" type="text" placeholder="Nama Siswa" name="nama_siswa"  value="<?= set_value('nama_siswa'); ?>">
			    <small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('nama_siswa'); ?></small>
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlFile1">Ijazah</label>
			    <input type="file" name="ijazah" class="form-control" id="exampleFormControlFile1">
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlFile1">Surat Keterangan Ujian</label>
			    <input type="file" name="sku" class="form-control" id="exampleFormControlFile1">
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlFile1">AKK</label>
			    <input type="file" name="akk" class="form-control" id="exampleFormControlFile1">
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlFile1">Kartu Keluarga</label>
			    <input type="file" name="kk" class="form-control" id="exampleFormControlFile1">
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlFile1">Pernyataan</label>
			    <input type="file" name="pernyataan" class="form-control" id="exampleFormControlFile1">
			  </div>
			  <button class="btn btn-info">Kirim</button>
			</form>
		</div>
	</div>
	<?php foreach ($kontak as $d): ?>
	<div class="col-lg-4 col-6">
		<div class="text-dark font-weight-bold">
          <h3 class="section-title">Informasi Sekolah</h3>
        </div>
        <h5><strong><?= $d->nama_sekolah ?></strong></h5>
        <small>Kota :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->kota ?></strong></p>
        <small>Alamat :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->alamat ?></strong></p>
        <small>No.Telepon :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->telepon ?></strong></p>
        <small>Email :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->email_sekolah ?></strong></p>
	</div>
	<?php endforeach ?>
</div>
