<div class="row mb-5">
	<div class="col-lg-8 col-6">
		<div class="col-lg-8">
			<form method="POST" action="<?= base_url('Page/contactUs'); ?>" enctype="multipart/form-data"> 
			  <div class="form-group">
			    <label for="exampleFormControlFile1">Nama</label>
			    <input class="form-control" type="text" placeholder="Nama Pengirim" name="nama"  value="<?= set_value('nama'); ?>">
			    <small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('nama'); ?></small>
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlFile1">Email</label>
			    <input class="form-control" type="email" placeholder="contoh : example@gmail.com" name="email"  value="<?= set_value('email'); ?>">
			    <small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('email'); ?></small>
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlTextarea1">Isi Pesan</label>
			    <textarea class="form-control" id="exampleFormControlTextarea1" rows="7" name="pesan"><?= set_value('pesan'); ?></textarea>
			     <small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('pesan'); ?></small>
			  </div>
			  <button class="btn btn-info">Kirim</button>
			</form>
		</div>
	</div>
	<?php foreach ($kontak as $d): ?>
	<div class="col-lg-4 col-6">
		<div class="text-dark font-weight-bold">
          <h3 class="section-title">Informasi Sekolah</h3>
        </div>
        <h5><strong><?= $d->nama_sekolah ?></strong></h5>
        <small>Kota :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->kota ?></strong></p>
        <small>Alamat :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->alamat ?></strong></p>
        <small>No.Telepon :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->telepon ?></strong></p>
        <small>Email :</small>
        <p style="font-size: 16px; margin-bottom: 5px;"><strong><?= $d->email_sekolah ?></strong></p>
	</div>
	<?php endforeach ?>
</div>

<div class="row mb-5">
	<iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJvW_fEyxIaC4RGcLn71bXck0&key=AIzaSyDsmhjS3vj-HLvmcMYNEV91pKS1zvoMor0" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
