<?php foreach ($struktur as $d): ?>
<!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">
            <img src="<?= base_url('assets/images/struktur_images/'.$d->foto) ?>" class="img-fluid" alt="">
          </div>

        </div>

        <div class="portfolio-description">
          <h2>Struktur Organisasi</h2>
          <p>
            <?= $d->deskripsi ?>
          </p>
        </div>
      </div>
    </section><!-- End Portfolio Details Section -->
<?php endforeach ?>   
  
