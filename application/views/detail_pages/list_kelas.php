<?php foreach ($kelas as $d): ?>
<!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details" style="padding-bottom: 100px;">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">
            <img src="<?= base_url('assets/images/kelas_images/'.$d->foto) ?>" class="img">
          </div>

          <div class="portfolio-info">
            <h3><?= $d->nama_kelas ?></h3>
            <ul class="mb-4">
              <li><?= $d->deskripsi_kelas ?></li>
            </ul>
          </div>

        </div>
      </div>
    </section><!-- End Portfolio Details Section -->
<?php endforeach ?>   
  
