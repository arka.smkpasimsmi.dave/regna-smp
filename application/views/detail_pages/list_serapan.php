<section id="team">
  <div class="container" data-aos="fade-up">
    <div class="row">
      <?php foreach ($bkk as $d): ?>
        <?php if ($d->kategori == "serapan"): ?>
      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="100">
          <div class="pic"><img src="<?= base_url('assets/images/bkk_images/'.$d->item) ?>" alt="" height="250" width="100%"></div>
          <a href="<?= base_url('Page/detailPageById/detailSerapan/'.$d->id) ?>"><h4><?= $d->judul ?></h4></a>
          <span class="text-left" style="font-style: normal;"><strong>Kota : </strong><?= $d->kota ?></span>
          <span class="text-left" style="font-style: normal;"><strong>Alamat : </strong><?= $d->alamat ?></span>
          <span class="text-left" style="font-style: normal;"><strong>Kontak : </strong><?= $d->no_telp.'|'.$d->email_perusahaan ?></span>
        </div>
      </div>
        <?php endif ?>
      <?php endforeach ?>
    </div>

  </div>
</section><!-- End Team Section -->