<?php foreach ($eskul as $d): ?>
<section id="about">
  <div class="container" data-aos="fade-up">
    <div class="row about-container">

      <div class="col-lg-6 content order-lg-1 order-2">
        <h2 class="title"><?= $d->judul ?></h2>
        <p><?= $d->deskripsi ?></p>
      </div>
      <div class="col-lg-6 background order-lg-2 order-1"  data-aos="fade-left" data-aos-delay="100"><img style="width:100%;max-height: 500px;object-fit: cover;object-position: center;" src="<?= base_url('assets/images/eskul_images/'.$d->item) ?>" alt="<?= base_url('assets/images/bkk_images/profil_default.png') ?>"></div>
    </div>
  </div>
</section><!-- End About Section -->
<?php endforeach ?>