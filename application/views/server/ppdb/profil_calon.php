<?php 
    if($this->session->userdata('role') !== 'admin')  {
    Redirect('Page/pageNotFound');
    } 
?>
<?php foreach($calon as $d) : ?>
<form method="POST" action="<?= base_url('Admin_ppdb/deleteCalon/'.$d->id) ?>" id="form-delete" ></form>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text" style="background-color:#6692e9;"></i>
                <div class="d-inline">
                    <h5>Berkas Siswa</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="../index.html"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('Admin'); ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">PPDB</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="text-center"> 
                    <a href="<?= base_url('assets/images/beranda_images/profil_default.png') ?>">
                        <img src="<?= base_url('assets/images/beranda_images/profil_default.png') ?>" class="rounded-circle" width="150" height="150">
                    </a>
                    <h4 class="card-title mt-10"><?= $d->nama_siswa ?></h4>
                </div>
            </div>
            <hr class="mb-0"> 
            <div class="card-body">
                <div class="text-center">
                    <button class="btn btn-danger" type="submit" id="btn-delete-submit">Hapus Data</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pills-deskripsi-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-deskripsi" aria-selected="false">Berkas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                </li>
            </ul>
            <!-- Tampilan Tab -->
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-deskripsi-tab">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 mb-20">
                            <input type="hidden" name="id" value="<?= $d->id; ?>">
                                <h6>Ijazah</h6>
                                <hr>
                                <a href="<?= base_url('assets/images/ppdb_images/'.$d->ijazah) ?>" class="single-popup-photo">
                                    <img style="width:100%;max-height: 250px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/ppdb_images/'.$d->ijazah) ?>" class="img-fluid rounded">
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 mb-20">
                                <h6>SKU</h6>
                                <hr>
                                <a href="<?= base_url('assets/images/ppdb_images/'.$d->sku) ?>" class="single-popup-photo">
                                    <img style="width:100%;max-height: 250px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/ppdb_images/'.$d->sku) ?>" class="img-fluid rounded">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 mb-20">
                                <h6>AKK</h6>
                                <hr>
                                <a href="<?= base_url('assets/images/ppdb_images/'.$d->akk) ?>" class="single-popup-photo">
                                    <img style="width:100%;max-height: 250px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/ppdb_images/'.$d->akk) ?>" class="img-fluid rounded">
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 mb-20">
                                <h6>KK</h6>
                                <hr>
                                <a href="<?= base_url('assets/images/ppdb_images/'.$d->kk) ?>" class="single-popup-photo">
                                    <img style="width:100%;max-height: 250px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/ppdb_images/'.$d->kk) ?>" class="img-fluid rounded">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 mb-20">
                                <h6>Pernyataan</h6>
                                <hr>
                                <a href="<?= base_url('assets/images/ppdb_images/'.$d->pernyataan) ?>" class="single-popup-photo">
                                    <img style="width:100%;max-height: 250px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/ppdb_images/'.$d->pernyataan) ?>" class="img-fluid rounded">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- End Tampilan Tab -->

            <!-- Edit Tab -->
            <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                    <form method="POST" action="<?= base_url('Admin_ppdb/postEditPpdb/'.$d->id) ?>" id="form-edit" enctype="multipart/form-data">
                    <!-- Edit Nama -->
                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mt-2">
                                    <label>Nama Siswa</label>
                                        <div class="input-group col-xs-12">
                                            <input type="text" class="form-control file-upload-info" name="nama_siswa" value="<?= $d->nama_siswa; ?>">
                                        </div>
                                </div>
                            </div> 
                    </div>
                    <!-- End Edit Nama -->
                    <!-- Row 1 -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <label>Ijazah</label>
                                        <div class="input-group col-xs-12">
                                            <input type="file" class="form-control file-upload-info" name="ijazah" >
                                            <input type="hidden" class="form-control file-upload-info" name="foto_lama_ijazah" value="<?= $d->ijazah; ?>">
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <label>SKU</label>
                                        <div class="input-group col-xs-12">
                                            <input type="file" class="form-control file-upload-info" name="sku" >
                                            <input type="hidden" class="form-control file-upload-info" name="foto_lama_sku" value="<?=$d->sku; ?>">
                                        </div>
                                </div>
                            </div>    
                        </div>
                    <!-- End Row 1 -->
                    <!-- Row 2 -->
                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <label>AKK</label>
                                        <div class="input-group col-xs-12">
                                            <input type="file" class="form-control file-upload-info" name="akk" >
                                            <input type="hidden" class="form-control file-upload-info" name="foto_lama_akk" value="<?= $d->akk; ?>">
                                        </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <label>KK</label>
                                        <div class="input-group col-xs-12">
                                            <input type="file" class="form-control file-upload-info" name="kk" >
                                            <input type="hidden" class="form-control file-upload-info" name="foto_lama_kk" value="<?= $d->kk; ?>">
                                        </div>
                                </div>
                            </div>    
                        </div>
                    <!-- End Row 2 -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mt-2">
                                    <label>Pernyataan</label>
                                        <div class="input-group col-xs-12">
                                            <input type="file" class="form-control file-upload-info" name="pernyataan" >
                                            <input type="hidden" class="form-control file-upload-info" name="foto_lama_pernyataan" value="<?=$d->pernyataan; ?>">
                                        </div>
                                        <small style="color:red;">Catatan : Biarkan kosong jika tidak ingin diubah*</small>
                                </div>
                            </div>
                        </div>   
                            <button class="btn btn-success" type="submit" id="btn-edit-submit">Update Data</button>
                        </div>   
                    </div>
            </div>
            <!-- End Edit Tab -->
            </form>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>