<main id="main">

    <!-- ======= Breadcrumbs Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2><?= $judul ?></h2>
          <ol>
            <li><a href="<?= base_url() ?>">Beranda</a></li>
            <li><?= $judul ?></li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs Section -->

    <section class="inner-page pt-4">
      <div class="container">
          <?php
            if ($this->uri->segment(3) == "detailStruktur") {
              $this->load->view('detail_pages/struktur_organisasi');
            }elseif ($this->uri->segment(3) == "ListKelas") {
              $this->load->view('detail_pages/list_kelas');
            }elseif ($this->uri->segment(3) == "ListFoto") {
              $this->load->view('detail_pages/list_foto');
            }elseif ($this->uri->segment(3) == "ListVideo") {
              $this->load->view('detail_pages/list_video');
            }elseif ($this->uri->segment(3) == "ListGuru") {
              $this->load->view('detail_pages/list_guru');
            }elseif ($this->uri->segment(3) == "ListPrestasi") {
              $this->load->view('detail_pages/list_prestasi');
            }elseif ($this->uri->segment(3) == "detailPpdb" || $this->uri->segment(2) == "insertPpdb") {
              $this->load->view('detail_pages/ppdb');
            }elseif ($this->uri->segment(3) == "detailKontak" || $this->uri->segment(2) == "contactUs") {
              $this->load->view('detail_pages/contact_us');
            }elseif ($this->uri->segment(3) == "Login") {
              $this->load->view('detail_pages/login');
            }elseif ($this->uri->segment(3) == "detailEskul") {
              $this->load->view('detail_pages/detail_eskul');
            }
           ?>
      </div>
    </section>

  </main><!-- End #main -->