 <?php foreach ($beranda as $d): ?>
 <!-- ======= Hero Section ======= -->
  <section id="hero" style="background: url(<?= base_url('assets/images/beranda_images/'.$d->foto_banner) ?>) top center;">
    <div class="col-md-6 col-sm-12 text-left ml-4" data-aos="zoom-in" data-aos-delay="100" style="margin-top: 200px;">
      <h1><?= $d->judul_banner ?></h1>
      <h2><?= $d->sub_judul_banner ?></h2>
    </div>
  </section><!-- End Hero Section -->
  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about">
      <div class="container" data-aos="fade-up">
        <div class="row about-container">

          <div class="col-lg-6 content order-lg-1 order-2">
            <h2 class="title">Sambutan Kepala Sekolah</h2>
            <p>
             <?= $d->deskripsi_sambutan ?>
            </p>
          </div>
          <div class="col-lg-6 background order-lg-2 order-1"  data-aos="fade-left" data-aos-delay="100"><img style="width:100%;max-height: 500px;object-fit: cover;object-position: center;" src="<?= base_url('assets/images/beranda_images/'.$d->foto_kepala_sekolah) ?>" alt="<?= base_url('assets/images/beranda_images/profil_default.png') ?>"></div>
        </div>
      </div>
    </section><!-- End About Section -->

    <!-- ======= Facts Section ======= -->
    <section id="facts">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">Visi & Misi</h3>
          <p class="section-description">Visi dan Misi Kami</p>
        </div>
        <div class="row">

          <div class="col-lg-6 col-6 text-center">
            <h4 class="text-dark">Visi</h4>
            <p class="text-left"><?= $d->deskripsi_visi ?></p>
          </div>

          <div class="col-lg-6 col-6 text-center">
            <h4 class="text-dark">Misi</h4>
            <p class="text-left"><?= $d->deskripsi_misi ?></p>
          </div>

        </div>

      </div>
    </section><!-- End Facts Section -->
    <?php endforeach ?>

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">Galeri Sekolah</h3>
          <p class="section-description">Berikut merupakan list galeri kegiatan sekolah</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter=".filter-foto">Foto</li>
              <li data-filter=".filter-video">Video</li>
            </ul>
          </div>
        </div>
        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

        <?php foreach ($galeri as $d):?>
          <?php if ($d->kategori == "foto"): ?>
            
          <div class="col-lg-4 col-md-6 portfolio-item filter-foto">
            <img height="265" width="100%" src="<?= base_url('assets/images/galeri_images/'.$d->item) ?>" alt="">
            <div class="portfolio-info">
              <h4><?= $d->judul ?></h4>
              <p><?= $d->deskripsi ?></p>
              <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php elseif($d->kategori == "video"): ?>
          <?php $url = $d->item;  ?>
          <?php $value = explode("v=", $url); ?>
          <?php $videoId = $value[1]; ?>
          <div class="col-lg-4 col-md-6 portfolio-item filter-video">
            <iframe width="100%" height="265px" src="https://www.youtube.com/embed/<?= $videoId; ?>"></iframe>
            <div class="portfolio-info">
              <h4><?= $d->judul ?></h4>
              <p><?= $d->deskripsi ?></p>
              <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          <?php endif ?>
        <?php endforeach ?>

        </div>
      </div>
    </section><!-- End Portfolio Section -->
    

    <!-- ======= Team Section ======= -->
    <section id="team">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3 class="section-title">Guru</h3>
          <p class="section-description">Berikut merupakan list guru-guru SMP Pasim Ar-Rayyan</p>
        </div>
        <div class="row">
          <?php foreach ($guru as $d): ?>
          <div class="col-lg-3 col-md-6">
            <div class="member" data-aos="fade-up" data-aos-delay="100">
              <div class="pic"><img src="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" alt="" height="250" width="100%"></div>
              <h4><?= $d->nama_guru ?></h4>
              <span><?= $d->mapel ?></span>
              <div class="social">
                <?php if ($d->facebook == " ") {
                            $facebook ="web.facebook.com";
                        }else{
                            $facebook ="$d->facebook";
                        } 
                    ?>
                <a href="<?= $facebook ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="<?= 'https://www.instagram.com/'.$d->instagram ?>" target="_blank"><i class="fa fa-instagram"></i></a>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact">
      <div class="container">
        <div class="section-header">
          <h3 class="section-title">Hubungi Kami</h3>
          <p class="section-description">Hubungi kami melalui kontak dibawah ini</p>
        </div>
      </div>

      <div class="container mt-5">
        <div class="row justify-content-center">

          <div class="col-lg-3 col-md-4">

            <div class="info">
              <?php foreach ($kontak as $d): ?>
              <div>
                <i class="fa fa-map-marker"></i>
                <p><?= $d->kota ?> <br><?= $d->alamat ?></p>
              </div>

              <div>
                <i class="fa fa-envelope"></i>
                <p><?= $d->email_sekolah ?></p>
              </div>

              <div>
                <i class="fa fa-phone"></i>
                <p><?= $d->telepon ?></p>
              </div>
              <?php endforeach ?> 
            </div>
          </div>
          <div class="col-lg-5 col-md-8">
            <div class="form">
              <form action="<?= base_url('Page/contactUs') ?>" method="POST">
                <div class="form-group">
                  <input type="text" name="nama" class="form-control" placeholder="Nama Pengirim" />
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email Pengirim"/>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="pesan" rows="5" placeholder="Isi Pesan"></textarea>
                </div>
                <div class="text-left"><button class="btn btn-info" type="submit">Kirim</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->
