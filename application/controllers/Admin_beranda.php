<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_beranda extends CI_Controller {

	public function index()
	{
		$title['title'] = 'Beranda';
		$data = [
			'beranda'	=> $this->crud->get('tb_m_beranda')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/beranda/index',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function postEditBeranda($id)
	{
		// $this->form_validation->set_rules('nama_sekolah','Nama Sekolah','required',['required' => 'Mohon masukan Nama Sekolah']);
		// $this->form_validation->set_rules('teks_logo','Teks Logo','required',['required' => 'Mohon masukan Teks Logo']);
		// $this->form_validation->set_rules('judul_banner','Judul Banner','required',['required' => 'Mohon masukan Judul Banner']);
		// $this->form_validation->set_rules('sub_judul_banner','Sub Judul Banner','required',['required' => 'Mohon masukan Sub Judul Banner']);
		// $this->form_validation->set_rules('nama_kepala_sekolah','Nama Kepala Sekolah','required',['required' => 'Mohon masukan Nama Kepala Sekolah']);

		if ($this->form_validation->run() == "false") {
			$this->session->set_flashdata('fail', 'Data gagal diperbarui!');
			Redirect('Admin_beranda');
		}else{
			$id 						= ['id' => $id];
			$nama_sekolah 				= $this->input->post('nama_sekolah');
			$ikon 						= $this->input->post('ikon');
			$teks_logo 					= $this->input->post('teks_logo');
			$judul_banner 				= $this->input->post('judul_banner');
			$sub_judul_banner 			= $this->input->post('sub_judul_banner');
			$foto_banner 				= $this->input->post('foto_banner');
			$nama_kepala_sekolah 		= $this->input->post('nama_kepala_sekolah');
			$foto_kepala_sekolah 		= $this->input->post('foto_kepala_sekolah');
			$deskripsi_sambutan 		= $this->input->post('deskripsi_sambutan');
			$deskripsi_visi 			= $this->input->post('deskripsi_visi');
			$deskripsi_misi 			= $this->input->post('deskripsi_misi');

			$lama_ikon 					= $this->input->post('lama_ikon');
			$lama_banner 				= $this->input->post('lama_banner');
			$lama_kepala_sekolah 		= $this->input->post('lama_kepala_sekolah');
			

			$config['upload_path']		= './assets/images/beranda_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $nama_sekolah.'-'.date('y-m-d');
			$this->load->library('upload', $config);	


				if ($ikon !== '') {

					if(!$this->upload->do_upload('ikon')){
						$foto_ikon	 	= $lama_ikon;
						$hapus_ikon 	= 'false';
		    		}else{
		    			$upload_data	= $this->upload->data();
		    			$foto_ikon 	  	= $upload_data['file_name'];
		    			$hapus_ikon		= 'true';
		   			}	
				}

				if ($foto_banner !== '') {

					if(!$this->upload->do_upload('foto_banner')){
						$foto_banner	= $lama_banner;
						$hapus_banner 	= 'false';
		    		}else{
		    			$upload_data	= $this->upload->data();
		    			$foto_banner 	= $upload_data['file_name'];
		    			$hapus_banner	= 'true';
		   			}	
				}

				if ($foto_kepala_sekolah !== '') {

					if(!$this->upload->do_upload('foto_kepala_sekolah')){
						$foto_kepala_sekolah	= $lama_kepala_sekolah;
						$hapus_kepala_sekolah 	= 'false';
		    		}else{
		    			$upload_data			= $this->upload->data();
		    			$foto_kepala_sekolah	= $upload_data['file_name'];
		    			$hapus_kepala_sekolah	= 'true';
		   			}	
				}

				// var_dump($foto_ikon);
				// var_dump($foto_banner);	
				// var_dump($foto_kepala_sekolah);

				
				$data = [
				'ikon'					=> $foto_ikon,
				'teks_logo'				=> $teks_logo,
				'foto_banner'			=> $foto_banner,
				'judul_banner'			=> $judul_banner,
				'sub_judul_banner'		=> $sub_judul_banner,
				'nama_sekolah'			=> $nama_sekolah,
				'foto_kepala_sekolah'	=> $foto_kepala_sekolah,
				'nama_kepala_sekolah'	=> $nama_kepala_sekolah,
				'deskripsi_sambutan'	=> $deskripsi_sambutan,
				'deskripsi_visi'		=> $deskripsi_visi,
				'deskripsi_misi'		=> $deskripsi_misi,
				'created_by'			=> 'ADMIN'
			];

			// var_dump($data);

			// if ($hapus_ikon == 'true') {
			// 	$this->crud->deletePhoto($lama_ikon,'beranda_images');
			// }elseif ($hapus_banner == 'true') {
			// 	$this->crud->deletePhoto($lama_banner,'beranda_images');
			// }elseif ($hapus_kepala_sekolah == 'true') {
			// 	$this->crud->deletePhoto($lama_kepala_sekolah,'beranda_images');
			// }

			$this->crud->edit($id,$data,'tb_m_beranda');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			Redirect('Admin_beranda');

	   	// 		if(!$this->upload->do_upload('foto_banner')){
					// $foto_banner	= $lama_banner;
					// $hapus_banner 	= 'false';
	    // 		}else{
	    // 			$upload_data	= $this->upload->data();
	    // 			$foto 	  		= $upload_data['file_name'];
	    // 			$hapus_banner	= 'true';
	   	// 		}

	   	// 		if(!$this->upload->do_upload('foto_kepala_sekolah')){
					// $foto_kepala_sekolah	= $lama_kepala_sekolah;
					// $hapus_kepala_sekolah	= 'false';
	    // 		}else{
	    // 			$upload_data			= $this->upload->data();
	    // 			$foto 	  				= $upload_data['file_name'];
	    // 			$hapus_kepala_sekolah	= 'true';
	   	// 		}
			
			// if ($ikon == '' && $foto_banner == '' && $foto_kepala_sekolah == '') {//kalau gaada = all
			// 	echo "GAADA";
			// 	$foto['ikon']			= $foto_lama['ikon'];
			// 	$foto['banner']		 	= $foto_lama['banner'];
			// 	$foto['kepala_sekolah'] = $foto_lama['kepala_sekolah'];
			// 	$hapus_ikon 			= 'false';
			// 	$hapus_banner 			= 'false';	
			// 	$hapus_kepala_sekolah 	= 'false';
			// }elseif ($ikon !== '' && $foto_banner !== '' && $foto_kepala_sekolah !== '') {//kalau semua = all
			// 	echo "SEMUA";
			// 	if ($this->upload->do_upload('ikon')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['ikon'] = $fileData['file_name'];
			// 	}
			// 	if ($this->upload->do_upload('foto_banner')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['banner'] = $fileData['file_name'];
			// 	}
			// 	if ($this->upload->do_upload('foto_kepala_sekolah')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['kepala_sekolah'] = $fileData['file_name'];
			// 	}
			// 	$hapus_ikon 			= 'true';
			// 	$hapus_banner 			= 'true';	
			// 	$hapus_kepala_sekolah 	= 'true';
			// }elseif ($ikon !== '' && $foto_banner == '' && $foto_kepala_sekolah == '') {//kalau ikon = single
			// 	echo "1";
			// 	if ($this->upload->do_upload('ikon')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['ikon'] = $fileData['file_name'];
			// 	}
			// 	$foto['banner']		 	= $foto_lama['banner'];
			// 	$foto['kepala_sekolah'] = $foto_lama['kepala_sekolah'];
			// 	$hapus_ikon 			= 'true';
			// 	$hapus_banner 			= 'false';	
			// 	$hapus_kepala_sekolah 	= 'false';
			// }elseif ($ikon == '' && $foto_banner == '' && $foto_kepala_sekolah !== '') {//kalau foto kepala sekolah = single
			// 	echo "1";
			// 	if ($this->upload->do_upload('foto_kepala_sekolah')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['kepala_sekolah'] = $fileData['file_name'];
			// 	}
			// 	$foto['banner'] 		= $foto_lama['banner'];
			// 	$foto['ikon'] 			= $foto_lama['ikon'];
			// 	$hapus_ikon 			= 'false';
			// 	$hapus_banner 			= 'false';	
			// 	$hapus_kepala_sekolah 	= 'true';
			// }elseif ($ikon == '' && $foto_banner !== '' && $foto_kepala_sekolah == '') {//kalau foto banner = single
			// 	echo "1";
			// 	if ($this->upload->do_upload('foto_banner')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['banner'] = $fileData['file_name'];
			// 	}
			// 	$foto['kepala_sekolah'] = $foto_lama['kepala_sekolah'];
			// 	$foto['ikon'] 			= $foto_lama['ikon'];
			// 	$hapus_ikon 			= 'false';
			// 	$hapus_banner 			= 'true';	
			// 	$hapus_kepala_sekolah 	= 'false';
			// }elseif ($ikon !== '' && $foto_banner !== '' && $foto_kepala_sekolah == '') {//kalau ikon & foto banner = double
			// 	echo "2";
			// 	if ($this->upload->do_upload('ikon')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['ikon'] = $fileData['file_name'];
			// 	}
			// 	if ($this->upload->do_upload('foto_banner')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['banner'] = $fileData['file_name'];
			// 	}
			// 	$foto['kepala_sekolah'] = $foto_lama['kepala_sekolah'];
			// 	$hapus_ikon 			= 'true';
			// 	$hapus_banner 			= 'true';	
			// 	$hapus_kepala_sekolah 	= 'false';
			// }elseif ($ikon !== '' && $foto_banner == '' && $foto_kepala_sekolah !== '') {//kalau ikon & foto kepala sekolah = double
			// 	echo "2";
			// 	if ($this->upload->do_upload('ikon')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['ikon'] = $fileData['file_name'];
			// 	}
			// 	if ($this->upload->do_upload('foto_kepala_sekolah')) {
			// 		$fileData	= $this->upload->data();
			// 		$foto['kepala_sekolah'] = $fileData['file_name'];
			// 	}
			// 	$foto['banner'] 		= $foto_lama['banner'];
			// 	$hapus_ikon 			= 'true';
			// 	$hapus_banner 			= 'true';	
			// 	$hapus_kepala_sekolah 	= 'false';
			// }elseif ($ikon == '' && $foto_banner !== '' && $foto_kepala_sekolah !== '') {//kalau foto banner & foto kepala sekolah = double
			// 	echo "2";
			// 	if ($this->upload->do_upload('foto_banner')) {
			// 		$foto['banner'] = $fileData['file_name'];
			// 	}
			// 	if ($this->upload->do_upload('foto_kepala_sekolah')) {
			// 		$foto['kepala_sekolah'] = $fileData['file_name'];
			// 	}
			// 	$foto['ikon'] 			= $foto_lama['ikon'];
			// 	$hapus_ikon 			= 'false';
			// 	$hapus_banner 			= 'true';	
			// 	$hapus_kepala_sekolah 	= 'true';
			// }
			
			
		}
	}
}
	