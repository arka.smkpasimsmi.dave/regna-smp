<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_sekolah extends CI_Controller {

	public function listKelas()
	{
		$title['title'] = 'Kelas';
		$data = [
			'kelas'	=> $this->crud->get('tb_m_kelas')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/profil/list_kelas',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function indexStruktur()
	{
		$title['title'] = 'Struktur';
		$data = [
			'struktur'	=> $this->crud->get('tb_m_struktur_organisasi')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/profil/struktur',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function insertKelas()
	{
		$this->form_validation->set_rules('nama_kelas','Nama Kelas', 'required',
    		['required' => 'Nama Kelas harus diisi!']);
		$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
    		['required' => 'Deskripsi harus diisi!']);

		if ($this->form_validation->run()== false) {
			$title['title'] = 'Kelas';
			$data = [
				'kelas'	=> $this->crud->get('tb_m_kelas')
				];

			$this->load->view('templates/server_partial/script_css',$title);
			$this->load->view('templates/server_partial/header');
			$this->load->view('templates/server_partial/sidebar');
			$this->load->view('server/front_end/profil/list_kelas',$data);
			$this->load->view('templates/server_partial/footer');
			$this->load->view('templates/server_partial/script_js');
		}else{
			$nama_kelas		= $this->input->post('nama_kelas');
			$deskripsi		= $this->input->post('deskripsi');

				$config['upload_path']		= './assets/images/kelas_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['file_name']		= $nama_kelas.'-'.date('y-m-d');
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('foto')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect('Profil_sekolah/listKelas');
				}else{
					$foto 	  = $this->upload->data('file_name');
					$data = [
						'nama_kelas'		=> $nama_kelas,
						'foto'				=> $foto,
						'deskripsi_kelas'	=> $deskripsi
					];
					$this->crud->insert($data,'tb_m_kelas');
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Profil_sekolah/listKelas');
			}
		}

	}

	public function detailKelas($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_kelas',$id)->row_array();
		$title['title'] = 'Detail Kelas | '.$nama['nama_kelas'];
		$data = [
			'kelas'	=> $this->crud->getById('tb_m_kelas',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/profil/kelas_detail',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function postEditKelas($ids) {
		$id 			= ['id' => $ids];
		$nama_kelas		= $this->input->post('nama_kelas');
		$deskripsi 		= $this->input->post('deskripsi');
		$foto 			= $this->input->post('foto');
		$foto_lama 		= $this->input->post('foto_lama');

		if ($foto !== '') {  
			$config['upload_path']		= './assets/images/kelas_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= 'struktur_sekolah-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}

			$data = [
				'foto'				=> $foto,
				'nama_kelas'		=> $nama_kelas,
				'deskripsi_kelas'	=> $deskripsi
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'kelas_images');
			}
			$this->crud->edit($id,$data,'tb_m_kelas');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Profil_sekolah/detailKelas/').$ids);	
		
	}

	public function deleteKelas($id)
	{
		$this->crud->delete($id,'tb_m_kelas');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Profil_sekolah/listKelas');
	}

	public function postEditStruktur($id)
	{
		$id 		= ['id' => $id];
		$foto_lama 	= $this->input->post('foto_lama');
		$deskripsi 	= $this->input->post('deskripsi');
		$foto 		= $this->input->post('foto');

		if ($foto !== '') {  
			$config['upload_path']		= './assets/images/struktur_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= 'struktur_sekolah-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}
			$data = [
				'foto'		=> $foto,
				'deskripsi' => $deskripsi	
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'struktur_images');
			}
			$this->crud->edit($id,$data,'tb_m_struktur_organisasi');
			$this->session->set_flashdata('success','Sukses Update data!');
			Redirect('Profil_sekolah/indexStruktur');
		}
}
