-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Okt 2020 pada 15.41
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfo_smp`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(10) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `auth`
--

INSERT INTO `auth` (`id`, `username`, `password`, `role`, `guru_id`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(6, 'admin@gmail.com', '$2y$10$nUag0VqGOGQJ0DU/oraYbexUNWjs2UZNtUerao6nVE7MqpTGHtP1q', 'admin', 0, '', '2020-09-12 04:32:39', NULL, '2020-10-13 04:58:08'),
(10, 'herni@gmail.com', '$2y$10$bltoa4ub/j36D0LFuyrHUeiKqOpZ2oX/1W7xAwkgY.zpmK8dfR4dG', 'guru', 22, '', '2020-09-15 08:59:49', NULL, NULL),
(11, 'haikal@gmail.com', '$2y$10$x0tAnsVze35tAWDFyX/A9u4roIu95YPJpxcriYhXXmrCej1yYZUxW', 'guru', 23, 'ADMIN', '2020-09-15 14:08:34', NULL, NULL),
(12, 'axa@gmail.com', '$2y$10$4iOYuV.A0axCI3NfRqaLrOCnaWZXvtGKZK3OE8hVeVFJ0Ad/oERsq', 'guru', 24, 'ADMIN', '2020-10-13 04:56:31', NULL, NULL),
(13, 'zakaria@gmail.com', '$2y$10$WxUs1vDRmEh.jU2RJb4BDO9AYAfBFzkVKj6w4XBRUP7A9jx82Lx9G', 'guru', 25, 'ADMIN', '2020-10-15 13:08:54', NULL, NULL),
(14, 'agung@gmail.com', '$2y$10$Fo/MQPSCGUwuYvvww8bvXOBgW68KVfYBIKu1RKCvj3wV8FPIY8ODC', 'guru', 26, 'ADMIN', '2020-10-15 13:09:53', NULL, NULL),
(15, 'nurkhabib@gmail.com', '$2y$10$qxQ3BnyJneYLCvWzNWACp.C32R9fJIfFOiXbWRoc7nQiL753.Y0O.', 'guru', 27, 'ADMIN', '2020-10-15 13:11:32', NULL, NULL),
(16, 'mulyani@gmail.com', '$2y$10$J.3fbLoSfhqXkHxmKhxuOejra.sX1WMElSl1Q50TEk2hdtis7bry.', 'guru', 28, 'ADMIN', '2020-10-15 13:12:27', NULL, NULL),
(17, 'sri@gmail.com', '$2y$10$1iZAziMtJq1tTB3K2G8DAuwn51JHepnI09XDjibkU.iNHhT0guQTG', 'guru', 29, 'ADMIN', '2020-10-15 13:13:35', NULL, NULL),
(18, 'yuni@gmail.com', '$2y$10$/zO3CqZ.2DVSvRGFRyS.ZuJBCrCkP.ptCN2yn.LHF1jy3.CgvRdsu', 'guru', 30, 'ADMIN', '2020-10-15 13:14:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_beranda`
--

CREATE TABLE `tb_m_beranda` (
  `id` int(11) NOT NULL,
  `ikon` varchar(128) NOT NULL,
  `teks_logo` varchar(128) NOT NULL,
  `foto_banner` varchar(128) NOT NULL,
  `judul_banner` varchar(128) NOT NULL,
  `sub_judul_banner` varchar(255) DEFAULT NULL,
  `nama_sekolah` varchar(128) NOT NULL,
  `foto_kepala_sekolah` varchar(128) NOT NULL,
  `nama_kepala_sekolah` varchar(128) NOT NULL,
  `deskripsi_sambutan` text NOT NULL,
  `deskripsi_visi` text NOT NULL,
  `deskripsi_misi` text NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_beranda`
--

INSERT INTO `tb_m_beranda` (`id`, `ikon`, `teks_logo`, `foto_banner`, `judul_banner`, `sub_judul_banner`, `nama_sekolah`, `foto_kepala_sekolah`, `nama_kepala_sekolah`, `deskripsi_sambutan`, `deskripsi_visi`, `deskripsi_misi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'SMP_ITech_Pasim_Ar_Rayyan-20-10-151.jpg', 'Pasim', 'SMP_ITech_Pasim_Ar_Rayyan-20-10-152.png', 'Sekolah Kreatif Berbasis IT', 'Rajin Menulis  Kerja Menjadi Praktis,  Malas Menulis,  Kerja Menjadi Pesimis', 'SMP ITech Pasim Ar Rayyan', 'Pasim_Creative_School-20-09-151.jpg', 'Jajat Sudrajat, S.Pd.I., M.Si', 'Assalamualaikum Warahmatullahi Wabarakatuh,\r\n\r\nSeraya memanjatkan puji syukur ke hadirat Tuhan yang Maha Esa, disertai perasaan bangga saya menuliskan kata sambutan Kepala sekolah, dalam rangka penerbitan Website sekolah. Di era global dan pesatnya Teknologi Informasi ini, tidak dipungkiri bahwa keberadaan sebuah website untuk suatu organisasi, termasuk SMP ITech Pasim Ar Rayyan, sangatlah penting. Wahana website dapat digunakan sebagai media penyebarluasan informasi-informasi dari sekolah, yang memang harus diketahui oleh stake holder secara luas. Disamping itu, website juga dapat menjadi sarana promosi sekolah yang cukup efektif. Berbagai kegiatan positif sekolah dapat diunggah, disertai gambar-gambar yang relevan, sehingga masyarakat dapat mengetahui prestasi-prestasi yang telah berhasil diraih oleh SMP ITech Pasim Ar Rayyan.\r\n\r\nSebagai media pembelajaran, website sekolah dapat digunakan oleh guru dan siswa untuk menuliskan berbagai artikel tentang pelajaran atau materi penting pelajaran yang bersangkutan. Bahkan guru dapat memberikan tugas-tugas Mandiri kepada peserta didik sehingga akan menunjang kegiatan pembelajaran berbasis Teknologi Informasi, Terima kasih.', 'Terwujudnya lembaga pendidikan berkarakter Qur’ani unggul  dalam prestasi  berwawasan teknologi\r\n', 'Menciptakan Siswa yang mempunyai pemahaman dan keyakinan Aqidah yang lurus, Ibadah yang baik. Akhlaq yang mulia dan berjiwa mandiri.\r\nMenciptakan Siswa yang memiliki mental juara, kompetitif, kreatif dan prestatif.\r\n', 'ADMIN', '2020-09-04 12:08:09', NULL, '2020-10-15 06:35:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_eskul`
--

CREATE TABLE `tb_m_eskul` (
  `id` int(11) NOT NULL,
  `item` varchar(128) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_eskul`
--

INSERT INTO `tb_m_eskul` (`id`, `item`, `judul`, `deskripsi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(11, '793fcedc4ab291f7374a27a83fb33132.jpg', 'Basket', 'Ekstrakulikuler basket di SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:19:06', NULL, NULL),
(12, 'Pramuka-20-10-15.jpg', 'Pramuka', 'Kegiatan eskul pramuka di SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:27:25', NULL, '2020-10-15 13:28:10'),
(13, 'b2bf53ca79c17b5e2ca4ee2583b30a60.jpg', 'Futsal', 'Eskul Futsal SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:30:55', NULL, NULL),
(14, '31d75f424a8a73c45301a16eda97964b.jpg', 'Karate', 'Esul Karate SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:33:03', NULL, NULL),
(15, 'd1630fa508fbccac3691b53e42cbe2a9.jpg', 'Seni', 'Eskul Seni SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:34:15', NULL, NULL),
(16, '280d571b6d06501216dafdf53ce6a717.jpg', 'Marawis', 'Eskul Marawis SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:36:36', NULL, NULL),
(17, '9a891326493be21037316cd62ca20e9d.jpg', 'Hafizh Qur\'an', 'Eskul Hafizh Qur\'an SMP Pasim Ar-Rayyan', 'ADMIN', '2020-10-15 13:38:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_galeri`
--

CREATE TABLE `tb_m_galeri` (
  `id` int(11) NOT NULL,
  `item` varchar(128) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `kategori` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_galeri`
--

INSERT INTO `tb_m_galeri` (`id`, `item`, `judul`, `deskripsi`, `kategori`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(23, 'Program_Gerak_Berbagi_Di_Bulan_Yang_Suci-20-10-15.jpeg', 'Program Gerak Berbagi Di Bulan Yang Suci', 'Kegiatan yang dilakukan di bulan suci Ramadhan 2020', 'foto', 'ADMIN', '2020-10-15 06:48:39', NULL, NULL),
(24, 'Program_Gerak_Berbagi_Di_Bulan_Yang_Suci-20-10-151.jpeg', 'Program Gerak Berbagi Di Bulan Yang Suci', 'Kegiatan yang dilakukan di bulan suci Ramadhan 2020', 'foto', 'ADMIN', '2020-10-15 06:49:55', NULL, NULL),
(25, 'Program_Gerak_Berbagi_Di_Bulan_Yang_Suci_Kegiatan-20-10-15.jpeg', 'Program Gerak Berbagi Di Bulan Yang Suci Kegiatan', 'Kegiatan yang dilakukan di bulan suci Ramadhan 2020', 'foto', 'ADMIN', '2020-10-15 06:50:16', NULL, NULL),
(26, 'Kegiatan_perpisahan_SMP_ITech_Pasim_Ar_Rayyan_2019-20-10-15.jpeg', 'Kegiatan perpisahan SMP ITech Pasim Ar Rayyan 2019', 'Foto guru-guru SMP ITech Pasim Ar Rayyan', 'foto', 'ADMIN', '2020-10-15 06:52:55', NULL, NULL),
(27, 'https://www.youtube.com/watch?v=sWuB7fzJCzw', 'Hemat Listrik', 'Video edukasi tentang menghemat listrik', 'video', 'ADMIN', '2020-10-15 06:54:25', NULL, '2020-10-15 06:55:56'),
(28, 'https://www.youtube.com/watch?v=YuAUZ6H2mWc', 'Kiko dan Cangkangnya', 'film pendek tentang kiko dan cangkangnya', 'video', 'ADMIN', '2020-10-15 06:55:15', NULL, '2020-10-15 06:56:09'),
(29, 'https://www.youtube.com/watch?v=a6GO8H3E-3g', 'Robb In Wood ', 'Filrm pendek digicon6 Asia\'s short movie contest', 'video', 'ADMIN', '2020-10-15 06:57:11', NULL, NULL),
(30, 'https://www.youtube.com/watch?v=Y1XVOaG3dpk', 'Karya Animasi 3d Bumper Robot', 'Animasi 3d bumper robot', 'video', 'ADMIN', '2020-10-15 06:57:56', NULL, NULL),
(31, 'https://www.youtube.com/watch?v=ZQKqTuYWcAI', 'Hardiknas 2020 \'BELAJAR DARI COVID 19\' | Pasim Sukabumi | Teaser', 'hardiknas 2020 tentang belajar dari covid 19', 'video', 'ADMIN', '2020-10-15 06:59:06', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_guru`
--

CREATE TABLE `tb_m_guru` (
  `id` int(11) NOT NULL,
  `nama_guru` varchar(128) NOT NULL,
  `mapel` varchar(128) NOT NULL,
  `tgl_masuk` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `facebook` varchar(128) DEFAULT NULL,
  `instagram` varchar(128) DEFAULT NULL,
  `gmail` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_guru`
--

INSERT INTO `tb_m_guru` (`id`, `nama_guru`, `mapel`, `tgl_masuk`, `foto`, `facebook`, `instagram`, `gmail`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(25, 'Zakaria S.E', 'IPS', '04/19/2015', 'db3b15f783c40067383288e4cc4d202f.jpg', NULL, NULL, '', 'ADMIN', '2020-10-15 13:08:53', NULL, NULL),
(26, 'Agung Basuki S.Pd', 'IPA', '01/07/2018', 'ff339ef7cb07fe83068379b4e7aab165.jpg', NULL, NULL, '', 'ADMIN', '2020-10-15 13:09:53', NULL, NULL),
(27, 'Nurkhabib S.Ag', 'PAI', '01/16/2009', '9d847ed4ac221bf20bb55bb25d5565d6.jpg', NULL, NULL, '', 'ADMIN', '2020-10-15 13:11:32', NULL, NULL),
(28, 'Drs. Mulyani', 'PAI', '12/15/2003', 'b4dc2a53e24c324a42261461c74e6575.jpg', NULL, NULL, '', 'ADMIN', '2020-10-15 13:12:26', NULL, NULL),
(29, 'Sri Pujiastuti S.Pd', 'PKn', '07/04/2014', '81c5b96ec4f3d4dd151badd4af8bf805.jpg', NULL, NULL, '', 'ADMIN', '2020-10-15 13:13:35', NULL, NULL),
(30, 'Yuni Hawini S.Pd', 'IPA', '11/20/2016', '5e24f6dabcb8cb25ff153d1c45ee2cfa.jpg', NULL, NULL, '', 'ADMIN', '2020-10-15 13:14:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_kelas`
--

CREATE TABLE `tb_m_kelas` (
  `id` int(11) NOT NULL,
  `nama_kelas` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `deskripsi_kelas` text NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(128) NOT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_m_kelas`
--

INSERT INTO `tb_m_kelas` (`id`, `nama_kelas`, `foto`, `deskripsi_kelas`, `created_dt`, `created_by`, `changed_dt`, `changed_by`) VALUES
(19, 'Kelas Mariam Al Asturabi', 'Kelas_Mariam_Al_Asturabi-20-10-15.jpeg', 'Tahun Pelajaran 2019-2020', '2020-10-15 06:42:22', '', NULL, NULL),
(20, 'Kelas Ibnu Haitam', 'Kelas_Ibnu_Haitam-20-10-15.jpeg', 'Tahun Pelajaran 2019-2020', '2020-10-15 06:42:38', '', NULL, NULL),
(21, 'Kelas Abbas Bin Firnas', 'Kelas_Abbas_Bin_Firnas-20-10-15.jpeg', 'Tahun Pelajaran 2019-2020', '2020-10-15 06:42:59', '', NULL, NULL),
(22, 'Kelas Al Jazari', 'Kelas_Al_Jazari-20-10-15.jpeg', 'Tahun Pelajaran 2019-2020', '2020-10-15 06:43:20', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_kontak`
--

CREATE TABLE `tb_m_kontak` (
  `id` int(11) NOT NULL,
  `nama_sekolah` varchar(128) NOT NULL,
  `kota` varchar(128) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(128) NOT NULL,
  `email_sekolah` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_kontak`
--

INSERT INTO `tb_m_kontak` (`id`, `nama_sekolah`, `kota`, `alamat`, `telepon`, `email_sekolah`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'SMP ITech Pasim Ar Rayyan', 'Sukabumi', 'Jalan Prana No.8A, Kec.Cikole', '0266-241000', 'smpitechpasim@gmail.com', 'ADMIN', '2020-09-10 04:52:28', NULL, '2020-10-15 07:02:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_pesan`
--

CREATE TABLE `tb_m_pesan` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `subject` varchar(128) DEFAULT NULL,
  `deskripsi` varchar(500) DEFAULT NULL,
  `status` varchar(128) DEFAULT NULL,
  `tipe_pesan` varchar(128) NOT NULL,
  `favorit` enum('true','false') NOT NULL,
  `sampah` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_pesan`
--

INSERT INTO `tb_m_pesan` (`id`, `nama`, `email`, `subject`, `deskripsi`, `status`, `tipe_pesan`, `favorit`, `sampah`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'Haikal', 'axarajand@gmail.com', 'Feedback Pengguna', 'loremloremlorem', 'belum_dibaca', 'penerima', 'false', 'false', 'Haikal', '2020-09-25 11:38:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_ppdb`
--

CREATE TABLE `tb_m_ppdb` (
  `id` int(11) NOT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `ijazah` varchar(128) NOT NULL,
  `sku` varchar(128) NOT NULL,
  `akk` varchar(128) NOT NULL,
  `kk` varchar(128) NOT NULL,
  `pernyataan` varchar(255) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_ppdb`
--

INSERT INTO `tb_m_ppdb` (`id`, `nama_siswa`, `ijazah`, `sku`, `akk`, `kk`, `pernyataan`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'Haikal', '47e8128275d6e5faa043ec0a1ac15f8a.jpg', '53e9f26efe8e27a52c03365d9b3506d0.jpg', '24b8724cd327b9127d1ef85451edaf66.jpg', 'c9b7571b42df20e8bcadb92a799b57ae.jpg', 'f869ecd75738c9dea902510ee27d5cad.jpg', 'Haikal', '2020-09-25 10:28:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_prestasi`
--

CREATE TABLE `tb_m_prestasi` (
  `id` int(11) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `nama_prestasi` varchar(128) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `tanggal_prestasi` varchar(128) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_prestasi`
--

INSERT INTO `tb_m_prestasi` (`id`, `foto`, `nama_prestasi`, `deskripsi`, `tanggal_prestasi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(20, 'Juara_Lomba_Adzan-07102020.jpeg', 'Juara Lomba Adzan', 'Prestasi lomba adzan ', '07/10/2020', '', '2020-10-15 07:07:30', NULL, NULL),
(21, 'Lomba_Kaligrafi-08202020.jpeg', 'Lomba Kaligrafi', 'Prestasi lomba kaligrafi ', '08/20/2020', '', '2020-10-15 07:08:18', NULL, NULL),
(22, 'Lomba_Proklamasi-09242020.jpeg', 'Lomba Proklamasi', 'Prestasi lomba proklamasi', '09/24/2020', '', '2020-10-15 07:09:03', NULL, NULL),
(23, 'Lomba_Poster_Kemerdekaan-09132020.jpeg', 'Lomba Poster Kemerdekaan', 'Prestasi poster kemerdekaan', '09/13/2020', '', '2020-10-15 07:09:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_m_struktur_organisasi`
--

CREATE TABLE `tb_m_struktur_organisasi` (
  `id` int(11) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `created_by` varchar(128) NOT NULL,
  `created_dt` timestamp NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(128) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_m_struktur_organisasi`
--

INSERT INTO `tb_m_struktur_organisasi` (`id`, `foto`, `deskripsi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'struktur_sekolah-20-10-15.png', '', 'ADMIN', '2020-08-25 04:19:23', NULL, '2020-10-15 06:39:39');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_beranda`
--
ALTER TABLE `tb_m_beranda`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_eskul`
--
ALTER TABLE `tb_m_eskul`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_galeri`
--
ALTER TABLE `tb_m_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_guru`
--
ALTER TABLE `tb_m_guru`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_kelas`
--
ALTER TABLE `tb_m_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_kontak`
--
ALTER TABLE `tb_m_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_pesan`
--
ALTER TABLE `tb_m_pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_ppdb`
--
ALTER TABLE `tb_m_ppdb`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_prestasi`
--
ALTER TABLE `tb_m_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_m_struktur_organisasi`
--
ALTER TABLE `tb_m_struktur_organisasi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tb_m_beranda`
--
ALTER TABLE `tb_m_beranda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_m_eskul`
--
ALTER TABLE `tb_m_eskul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tb_m_galeri`
--
ALTER TABLE `tb_m_galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `tb_m_guru`
--
ALTER TABLE `tb_m_guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `tb_m_kelas`
--
ALTER TABLE `tb_m_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `tb_m_kontak`
--
ALTER TABLE `tb_m_kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_m_pesan`
--
ALTER TABLE `tb_m_pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_m_ppdb`
--
ALTER TABLE `tb_m_ppdb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_m_prestasi`
--
ALTER TABLE `tb_m_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `tb_m_struktur_organisasi`
--
ALTER TABLE `tb_m_struktur_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
